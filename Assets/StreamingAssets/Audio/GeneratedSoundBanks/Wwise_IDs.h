/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID BULLET_EXPLODE = 3665756447U;
        static const AkUniqueID GAME_MUSIC_PLAY = 3981716704U;
        static const AkUniqueID GAME_MUSIC_STOP = 2325078882U;
        static const AkUniqueID NUKE_EXPLODE = 718413920U;
        static const AkUniqueID PLAY_AMBIENCE = 278617630U;
        static const AkUniqueID TANK_CHARGING_RESET = 3297342873U;
        static const AkUniqueID TANK_CHARGING_START = 373210848U;
        static const AkUniqueID TANK_CHARGING_STOP = 3805063692U;
        static const AkUniqueID TANK_DRIVING_START = 3269807044U;
        static const AkUniqueID TANK_DRIVING_STOP = 2049939320U;
        static const AkUniqueID TANK_EXPLODE = 4280294707U;
        static const AkUniqueID TANK_IDLING_START = 3880107934U;
        static const AkUniqueID TANK_IDLING_STOP = 4174210110U;
        static const AkUniqueID TANK_SHOOTING = 1948913527U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID CHARGE_TIME = 3308177379U;
        static const AkUniqueID DAMAGE = 1786804762U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
        static const AkUniqueID TANK_PITCHBEND = 1258990719U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID THE_BANK = 4203425823U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
