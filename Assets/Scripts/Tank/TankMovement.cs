﻿using UnityEngine;

public class TankMovement : MonoBehaviour
{
    public int m_PlayerNumber = 1;         
    public float m_Speed = 12f;            
    public float m_TurnSpeed = 180f;       
    //public AudioSource m_MovementAudio;    
    //public AudioClip m_EngineIdling;       
    //public AudioClip m_EngineDriving;      
    public float m_PitchRange = 0.2f;

    
    private string m_MovementAxisName;     
    private string m_TurnAxisName;         
    private Rigidbody m_Rigidbody;         
    private float m_MovementInputValue;    
    private float m_TurnInputValue;        
    private float m_OriginalPitch;         


    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
    }


    private void OnEnable ()
    {
        m_Rigidbody.isKinematic = false;
        // can't drive yet
        m_MovementInputValue = 0f;
        m_TurnInputValue = 0f;
    }


    private void OnDisable ()
    {
        //m_Rigidbody.isKinematic = true;
    }


    private void Start()
    {
        m_MovementAxisName = "Vertical" + m_PlayerNumber;
        m_TurnAxisName = "Horizontal" + m_PlayerNumber;
        if(m_PlayerNumber == 1)
            m_OriginalPitch = Random.Range(50, 100);
        else
            m_OriginalPitch = Random.Range(-50, -100);
        //m_OriginalPitch = m_MovementAudio.pitch;
    }
    

    private void Update()
    {
        // Store the player's input and make sure the audio for the engine is playing.
        m_MovementInputValue = Input.GetAxis(m_MovementAxisName);
        m_TurnInputValue = Input.GetAxis(m_TurnAxisName);

        EngineAudio();

    }

    bool idlePost, drivePost;
    private void EngineAudio()
    {
        AkSoundEngine.SetRTPCValue("tank_pitchbend", m_OriginalPitch, gameObject);

        // Play the correct audio clip based on whether or not the tank is moving and what audio is currently playing.
        // player isn't moving!!
        if (Mathf.Abs(m_MovementInputValue) < 0.1f && Mathf.Abs(m_TurnInputValue) < 0.1f)
        {
            if (!idlePost)
            {
                //AkSoundEngine.PostEvent("tank_driving_STOP", gameObject);
                //AkSoundEngine.PostEvent("tank_idling_Start", gameObject);
                idlePost = true;
                drivePost = false;
            }
            
            /*
            // idling
            if(m_MovementAudio.clip == m_EngineDriving)
            {
                m_MovementAudio.clip = m_EngineIdling;
                m_MovementAudio.pitch = Random.Range(m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
                m_MovementAudio.Play();
            }
            */
        }
        else
        {
            if(!drivePost)
            {
                //AkSoundEngine.PostEvent("tank_driving_Start", gameObject);
                //AkSoundEngine.PostEvent("tank_idling_STOP", gameObject);
                idlePost = false;
                drivePost = true;
            }
            
            /*
            // driving
            if (m_MovementAudio.clip == m_EngineIdling)
            {
                m_MovementAudio.clip = m_EngineDriving;
                m_MovementAudio.pitch = Random.Range(m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
                m_MovementAudio.Play();
            }
            */
        }
        Move();
        Turn();
    }


    private void FixedUpdate()
    {
        // Move and turn the tank.
        //Move();
        //Turn();
    }

    Vector3 movement;
    private void Move()
    {
        // Adjust the position of the tank based on the player's input.
        movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;
        m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
    }


    float turn;
    Quaternion turnRotation;
    private void Turn()
    {
        // Adjust the rotation of the tank based on the player's input.
        turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;
        turnRotation = Quaternion.Euler(0f, turn, 0f);

        m_Rigidbody.MoveRotation(m_Rigidbody.rotation * turnRotation);
    }
}