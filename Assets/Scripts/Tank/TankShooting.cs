﻿using UnityEngine;
using UnityEngine.UI;

public class TankShooting : MonoBehaviour
{
    public int m_PlayerNumber = 1;       
    public Rigidbody m_Shell;
    public GameObject m_Shell_Obj;
    public Transform m_FireTransform;    
    public Slider m_AimSlider;           
    //public AudioSource m_ShootingAudio;  
    //public AudioClip m_ChargingClip;     
    //public AudioClip m_FireClip;         
    public float m_MinLaunchForce = 15f; 
    public float m_MaxLaunchForce = 30f; 
    public float m_MaxChargeTime = 0.75f;

    private float m_OriginalPitch;

    private string m_FireButton;         
    private float m_CurrentLaunchForce;  
    private float m_ChargeSpeed;         
    private bool m_Fired;                


    private void OnEnable()
    {
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_AimSlider.value = m_MinLaunchForce;
    }


    private void Start()
    {
        m_FireButton = "Fire" + m_PlayerNumber;

        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;

        if (m_PlayerNumber == 1)
            m_OriginalPitch = Random.Range(50, 100);
        else
            m_OriginalPitch = Random.Range(-50, -100);
    }
    

    private void Update()
    {
        // Track the current state of the fire button and make decisions based on the current launch force.
        m_AimSlider.value = m_MinLaunchForce;


        if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)
        {
            // max charged, but not yet fired
            m_CurrentLaunchForce = m_MaxLaunchForce;

            Fire();
        }
        else if(Input.GetButtonDown(m_FireButton))
        {
            // pressed fire button for 1st time?
            m_Fired = false;
            m_CurrentLaunchForce = m_MinLaunchForce;

            // play charging
            
        }
        else if(Input.GetButton(m_FireButton) && !m_Fired)
        {
            AkSoundEngine.SetRTPCValue("tank_pitchbend", m_OriginalPitch, gameObject);
            // holding fire button?? & not fired yet
            if (m_CurrentLaunchForce == m_MinLaunchForce)
                AkSoundEngine.PostEvent("tank_charging_Start", gameObject);

            // charging!!!
            m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;
            m_AimSlider.value = m_CurrentLaunchForce;

            AkSoundEngine.SetRTPCValue("charge_time", m_CurrentLaunchForce/m_MaxLaunchForce*100, gameObject);
        }
        else if(Input.GetButtonUp(m_FireButton) && !m_Fired)
        {
            // we released the button, nhaving not fired yet
            Fire();
        }

        
    }


    private void Fire()
    {
        AkSoundEngine.SetRTPCValue("tank_pitchbend", m_OriginalPitch, gameObject);
        Debug.Log(m_OriginalPitch);
        // Instantiate and launch the shell.
        m_Fired = true;

        GameObject shellObj = Instantiate(m_Shell_Obj, m_FireTransform.position, m_FireTransform.rotation) as GameObject;
        if (Random.Range(0, 10) >= 9)
        {
            shellObj.transform.position += m_FireTransform.forward * 2;
            shellObj.transform.localScale = new Vector3(5, 5, 5);
            shellObj.GetComponent<ShellExplosion>().m_ExplosionForce *= 3f;
            shellObj.GetComponent<ShellExplosion>().m_ExplosionRadius *= 1.5f;
            shellObj.GetComponent<ShellExplosion>().m_MaxDamage *= 1.1f;
            shellObj.GetComponent<ShellExplosion>().m_MaxLifeTime *= 1.4f;
            shellObj.GetComponent<Rigidbody>().velocity = m_CurrentLaunchForce * 1.4f * m_FireTransform.forward + 1.4f * GetComponent<Rigidbody>().velocity;
            shellObj.GetComponent<Rigidbody>().mass = 4;
        }
        else
        {
            shellObj.GetComponent<Rigidbody>().velocity = m_CurrentLaunchForce * m_FireTransform.forward + 1.4f * GetComponent<Rigidbody>().velocity;
        }
       
        //Rigidbody shellInstance = Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;
        //shellInstance.GetComponent<GameObject>().transform.localScale = 10 * Vector3.one;
        //shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward + GetComponent<Rigidbody>().velocity;

        // stop charging sound
        //AkSoundEngine.PostEvent("tank_charging_STOP", gameObject);
        // play shooting sound
        AkSoundEngine.PostEvent("tank_shooting", gameObject);
        // reset charge sound pitch bend
        AkSoundEngine.PostEvent("tank_charging_reset", gameObject);
        m_CurrentLaunchForce = m_MinLaunchForce;
    }
    
    public void ResetChargeSound()
    {
        Debug.Log("reset charge sound?");
        //AkSoundEngine.PostEvent("tank_charging_STOP", gameObject);
        AkSoundEngine.PostEvent("tank_charging_reset", gameObject);
    }
}