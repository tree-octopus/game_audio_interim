﻿using UnityEngine;

public class ShellExplosion : MonoBehaviour
{
    public LayerMask m_TankMask;
    public ParticleSystem m_ExplosionParticles;       
    //public AudioSource m_ExplosionAudio;              
    public float m_MaxDamage = 100f;                  
    public float m_ExplosionForce = 1000f;            
    public float m_MaxLifeTime = 2f;                  
    public float m_ExplosionRadius = 5f;              


    private void Start()
    {
        Destroy(gameObject, m_MaxLifeTime);
    }


    private void OnTriggerEnter(Collider other)
    {
        // Find all the tanks in an area around the shell and damage them.
        Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_TankMask);

        for(int i = 0; i < colliders.Length; i++)
        {
            Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();
            if (!targetRigidbody)
                continue;
            targetRigidbody.AddExplosionForce(m_ExplosionForce, transform.position, m_ExplosionRadius);

            TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth>();
            if (!targetHealth)
                continue;

            float damage = CalculateDamage(targetRigidbody.transform.position);

            targetHealth.TakeDamage(damage);
        }

        m_ExplosionParticles.transform.parent = null;
        m_ExplosionParticles.Play();
        // play shell explosion audio
        //AkSoundEngine.PostEvent("bullet_explode", gameObject);
        Destroy(m_ExplosionParticles.gameObject, m_ExplosionParticles.main.duration);
    }

    private void OnCollisionEnter(Collision other)
    {
        // Find all the tanks in an area around the shell and damage them.
        Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_TankMask);

        for (int i = 0; i < colliders.Length; i++)
        {
            Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();
            if (!targetRigidbody)
                continue;
            targetRigidbody.AddExplosionForce(m_ExplosionForce, transform.position, m_ExplosionRadius);
            //targetRigidbody.AddForce(Vector3.up * 10, ForceMode.Impulse);
            TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth>();
            if (!targetHealth)
                continue;

            float damage = CalculateDamage(targetRigidbody.transform.position);

            targetHealth.TakeDamage(damage);
        }

        m_ExplosionParticles.transform.parent = null;
        for(int i = 0; i < other.contacts.Length; i++)
        {
            GameObject tempExplo = Instantiate(m_ExplosionParticles.gameObject, other.contacts[0].point, Quaternion.identity);
            m_ExplosionParticles.transform.position = other.contacts[0].point;
            m_ExplosionParticles.Play();
            // play shell explosion audio
            AkSoundEngine.PostEvent("bullet_explode", gameObject);
            //Destroy(m_ExplosionParticles.gameObject, m_ExplosionParticles.main.duration);
            Destroy(tempExplo, tempExplo.GetComponent<ParticleSystem>().main.duration);
        }
        Destroy(m_ExplosionParticles.gameObject, m_ExplosionParticles.main.duration);
        //Destroy(gameObject);
    }


    private float CalculateDamage(Vector3 targetPosition)
    {
        // Calculate the amount of damage a target should take based on it's position.

        return Mathf.Max(0f, m_MaxDamage * (m_ExplosionRadius - Vector3.Magnitude(targetPosition - transform.position)) / m_ExplosionRadius);
    }
}